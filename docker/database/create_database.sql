-- \set ON_ERROR_STOP on

CREATE DATABASE flask;

\c flask

CREATE TABLE IF NOT EXISTS users(
   id serial PRIMARY KEY,
   username VARCHAR (32) UNIQUE NOT NULL,
   password_hash VARCHAR (128)
);

CREATE TABLE IF NOT EXISTS product(
   id serial PRIMARY KEY,
   user_id INTEGER REFERENCES users(id),
   title VARCHAR (50),
   price NUMERIC (10, 2),
   description VARCHAR (255),
   image_url VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS cartorder(
   id serial PRIMARY KEY,
   user_id INTEGER REFERENCES users(id),
   amount NUMERIC (10, 2),
   dateTime TIMESTAMP
);

CREATE TABLE IF NOT EXISTS cart(
   id serial PRIMARY KEY,
   cart_order_id INTEGER REFERENCES cartorder(id),
   title VARCHAR (50),
   price NUMERIC (10, 2),
   quantity INTEGER
);

CREATE TABLE IF NOT EXISTS favorite(
   id serial PRIMARY KEY,
   user_id INTEGER REFERENCES users(id),
   product_id INTEGER REFERENCES product(id),
   is_favorite Boolean
);

-- password: bib
INSERT INTO users (username, password_hash) VALUES('bibo', '$6$rounds=656000$sd3XxWPVPlCKrvV0$SPINbNk.NTNp7AVxU3K6mnu2tc0ERkbaxHB9F9uLVjYpc1FMD2irYUT7CSFgappBL6dBUfBBLkvaMPVM9ZT8v1') ON CONFLICT (id) DO NOTHING;
INSERT INTO product (user_id, title, price, description, image_url) VALUES(1, 'Hamster Bibo', 4500.12, 'This is a rare hamster.', 'https://www.zooroyal.de/magazin/wp-content/uploads/2017/04/hamster-760x560.jpg') ON CONFLICT (id) DO NOTHING;

