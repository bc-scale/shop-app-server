# -*- coding: utf8 -*-

from flask import Blueprint

users = Blueprint(
  'api',
  __name__,
  static_folder='static',
  template_folder='templates'
)

from . import routes, models
